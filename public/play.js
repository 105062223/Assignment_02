var playState = {
	create: function() {
		this.cursor = game.input.keyboard.createCursorKeys();
		
		//soundeffects
		this.dieSound = game.add.audio('die');
		this.footstepSound = game.add.audio('footstep');
		this.hitSound = game.add.audio('hit');
		this.music = game.add.audio('music');
		this.music.volume = 0.5;
		this.music.loop = true;
		this.music.play();
		
		//firstBlock
		this.firstBlock = game.add.sprite(165,300,'block');
		game.physics.arcade.enable(this.firstBlock);
		this.firstBlock.body.immovable = true;
		this.firstBlock.body.checkCollision.down = false;
		this.firstBlock.body.checkCollision.left = false;
		this.firstBlock.body.checkCollision.right = false;
		this.firstBlock.body.velocity.y = -80;
		this.firstBlock.checkWorldBounds = true;
		this.firstBlock.outOfBoundsKill = true;
		
		//platforms
		this.blocks = game.add.group();
		this.blocks.enableBody = true;
		this.blocks.createMultiple(9,'block');
		this.blocks.setAll('body.immovable',true);
		this.blocks.setAll('body.checkCollision.down',false);
		this.blocks.setAll('body.checkCollision.left',false);
		this.blocks.setAll('body.checkCollision.right',false);
		this.blocks.setAll('checkWorldBounds',true);
		this.blocks.setAll('outOfBoundsKill',true);
		
		this.spikes = game.add.group();
		this.spikes.enableBody = true;
		this.spikes.createMultiple(3,'spike');
		this.spikes.setAll('body.immovable',true);
		this.spikes.setAll('body.checkCollision.down',false);
		this.spikes.setAll('body.checkCollision.left',false);
		this.spikes.setAll('body.checkCollision.right',false);
		this.spikes.setAll('checkWorldBounds',true);
		this.spikes.setAll('outOfBoundsKill',true);
		
		this.conveyors_left = game.add.group();
		this.conveyors_left.enableBody = true;
		this.conveyors_left.createMultiple(3,'conveyor_left');
		this.conveyors_left.setAll('body.immovable',true);
		this.conveyors_left.setAll('body.checkCollision.down',false);
		this.conveyors_left.setAll('body.checkCollision.left',false);
		this.conveyors_left.setAll('body.checkCollision.right',false);
		this.conveyors_left.setAll('checkWorldBounds',true);
		this.conveyors_left.setAll('outOfBoundsKill',true);
		
		this.conveyors_right = game.add.group();
		this.conveyors_right.enableBody = true;
		this.conveyors_right.createMultiple(3,'conveyor_right');
		this.conveyors_right.setAll('body.immovable',true);
		this.conveyors_right.setAll('body.checkCollision.down',false);
		this.conveyors_right.setAll('body.checkCollision.left',false);
		this.conveyors_right.setAll('body.checkCollision.right',false);
		this.conveyors_right.setAll('checkWorldBounds',true);
		this.conveyors_right.setAll('outOfBoundsKill',true);

		this.trampolines = game.add.group();
		this.trampolines.enableBody = true;
		this.trampolines.createMultiple(3,'trampoline');
		this.trampolines.setAll('body.immovable',true);
		this.trampolines.setAll('body.checkCollision.down',false);
		this.trampolines.setAll('body.checkCollision.left',false);
		this.trampolines.setAll('body.checkCollision.right',false);
		this.trampolines.setAll('checkWorldBounds',true);
		this.trampolines.setAll('outOfBoundsKill',true);
		
		game.time.events.loop(900,this.addplatform,this);
		
		//ceiling
		this.ceiling = game.add.sprite(0, 25, 'ceiling');
		game.physics.arcade.enable(this.ceiling);
		this.ceiling.body.immovable = true;
		
		//walls
		this.leftwall = game.add.sprite(0, 25, 'wall');
		this.rightwall = game.add.sprite(382, 25, 'wall');
		game.physics.arcade.enable(this.leftwall);
		game.physics.arcade.enable(this.rightwall);
		this.leftwall.body.immovable = true;
		this.rightwall.body.immovable = true;
		
		//player
		this.player = game.add.sprite(game.width/2, 42, 'player');
		this.player.frame = 8;
		game.physics.arcade.enable(this.player);
		this.player.body.gravity.y = 500;
		this.player.animations.add('rightwalk', [9,10,11,12], 15, true);
		this.player.animations.add('leftwalk', [0,1,2,3], 15, true);
		this.player.animations.add('fall', [36,37,38,39], 15, true);
		this.player.falling = false;
		this.player.invincible = false;
		
		this.lifetext = game.add.text(0,0, 'Life: 12', {font: '20px Arial', fill: '#ffff00' });
		this.life = 12;
		this.scoreLabel = game.add.text(250, 0, 'Level: 0', { font: '20px Arial', fill: '#ffff00' });
		game.global.score = 0;
		game.time.events.loop(3000,this.addScore,this);
	},
	update: function() {
		game.physics.arcade.collide(this.player, this.leftwall);
		game.physics.arcade.collide(this.player, this.rightwall);
		game.physics.arcade.collide(this.player, this.firstBlock, this.steponBlock, null, this);
		game.physics.arcade.collide(this.player, this.blocks, this.steponBlock, null, this);
		game.physics.arcade.collide(this.player, this.spikes, this.steponSpike, null, this);
		game.physics.arcade.collide(this.player, this.conveyors_left, this.shiftleft, null, this);
		game.physics.arcade.collide(this.player, this.conveyors_right, this.shiftright, null, this);
		game.physics.arcade.collide(this.player, this.trampolines, this.jump, null, this);
		game.physics.arcade.collide(this.player, this.ceiling, this.hitCeiling, null, this);
		
		this.movePlayer();
		
		if(!this.player.inWorld) {this.playerFallout();}
		if(this.life <= 0) {this.playerDie();}
	},
	addplatform: function() {
		var rand = Math.random()*50;
		
		if(rand < 20) {
			var block = this.blocks.getFirstDead();
			if(!block) {return;}
			block.reset(game.rnd.integerInRange(18,287),400);
			block.body.velocity.y = -80;
		}
		else if(rand < 30) {
			var spike = this.spikes.getFirstDead();
			if(!spike) {return;}
			spike.reset(game.rnd.integerInRange(18,287),400);
			spike.body.velocity.y = -80;
		}
		else if(rand < 35) {
			var conveyor_left = this.conveyors_left.getFirstDead();
			if(!conveyor_left) {return;}
			conveyor_left.reset(game.rnd.integerInRange(18,287),400);
			conveyor_left.animations.add('shiftLeft', [0,1,2,3], 16, true);
			conveyor_left.play('shiftLeft');
			conveyor_left.body.velocity.y = -80;
		}
		else if(rand < 40) {
			var conveyor_right = this.conveyors_right.getFirstDead();
			if(!conveyor_right) {return;}
			conveyor_right.reset(game.rnd.integerInRange(18,287),400);
			conveyor_right.animations.add('shiftRight', [0,1,2,3], 16, true);
			conveyor_right.play('shiftRight');
			conveyor_right.body.velocity.y = -80;
		}
		else if(rand < 50) {
			var trampoline = this.trampolines.getFirstDead();
			if(!trampoline) {return;}
			trampoline.reset(game.rnd.integerInRange(18,287),400);
			trampoline.animations.add('hop', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 120);
			trampoline.frame = 3;
			trampoline.body.velocity.y = -80;
		}
	},
	playerFallout: function() {
		this.dieSound.volume = 0.5;
		this.dieSound.play();
		this.music.stop();
		game.state.start('name');
	},
	playerDie: function() {
		this.music.stop();
		game.state.start('name');
	},
	steponBlock: function() {
		if(this.player.falling) {
			this.player.falling = false;
			if(!this.player.invincible){
				this.life += 1;
				if(this.life > 12) {
					this.life = 12;
				}
				this.lifetext.text = 'Life: ' + this.life;
				this.footstepSound.play();
			}
		}
	},
	steponSpike: function() {
		if(this.player.falling) {
			this.player.falling = false;
			if(!this.player.invincible){
				this.life -= 5;
				this.lifetext.text = 'Life: ' + this.life;
				this.hitSound.play();
				game.camera.flash(0xff0000, 100);
			}
		}
	},
	shiftleft: function() {
		if(this.player.falling) {
			this.player.falling = false;
			if(!this.player.invincible){
				this.life += 1;
				if(this.life > 12) {
					this.life = 12;
				}
				this.lifetext.text = 'Life: ' + this.life;
				this.footstepSound.play();
			}
		}
		if(this.player.body.x != 18){
			this.player.body.x -= 2;
		}
	},
	shiftright: function() {
		if(this.player.falling) {
			this.player.falling = false;
			if(!this.player.invincible){
				this.life += 1;
				if(this.life > 12) {
					this.life = 12;
				}
				this.lifetext.text = 'Life: ' + this.life;
				this.footstepSound.play();
			}
		}
		if(this.player.body.x != 350){
			this.player.body.x += 2;
		}
	},
	jump: function(player,trampoline) {
		if(this.player.falling) {
			this.player.falling = false;
			if(!this.player.invincible){
				this.life += 1;
				if(this.life > 12) {
					this.life = 12;
				}
				this.lifetext.text = 'Life: ' + this.life;
				this.footstepSound.play();
			}
		}
		trampoline.animations.play('hop');
		this.player.body.velocity.y = -200;
	},
	hitCeiling: function() {
		if(!this.player.invincible){
			this.life -= 5;
			this.lifetext.text = 'Life: ' + this.life;
			this.hitSound.play();
			game.camera.flash(0xff0000, 100);
		}
		this.player.invincible = true;
		game.time.events.add(390,this.notInvincible,this);
	},
	notInvincible: function() {
		this.player.invincible = false;
	},
	addScore: function() {
		game.global.score += 1;
		this.scoreLabel.text = 'Level: ' + game.global.score;
	},
	movePlayer: function() {
		if (this.cursor.left.isDown) {
            this.player.body.velocity.x = -200;
            this.player.animations.play('leftwalk');
        }
        else if (this.cursor.right.isDown) { 
            this.player.body.velocity.x = 200;
            this.player.animations.play('rightwalk');
        }
		else if(!this.player.body.touching.down) {
			this.player.body.velocity.x = 0;
			this.player.animations.play('fall');
		}
		else {
			this.player.body.velocity.x = 0;
			this.player.frame = 8;
		}
		
		if(this.player.body.velocity.y > 0) {
			this.player.falling = true;
		}
	}
};