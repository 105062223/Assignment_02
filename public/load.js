var loadState = {
	preload: function() {
		var loadingLabel = game.add.text(game.width/2,200,'Loading...',{font:'30px Arial',fill: '#ffffff'});
		loadingLabel.anchor.setTo(0.5,0.5);
		
		var progressBar = game.add.sprite(game.width/2,240,'progressBar');
		progressBar.anchor.setTo(0.5,0.5);
		game.load.setPreloadSprite(progressBar);
		
		//Load game sprites
		game.load.image('ceiling', 'assets/ceiling.png');
		game.load.image('wall', 'assets/wall.png');
		game.load.image('block', 'assets/normal.png');
		game.load.image('spike', 'assets/nails.png');
		
		//Load spritesheets
		game.load.spritesheet('player', 'assets/player.png', 32, 32);
		game.load.spritesheet('conveyor_left', 'assets/conveyor_left.png', 96, 16);
		game.load.spritesheet('conveyor_right', 'assets/conveyor_right.png', 96, 16);
		game.load.spritesheet('trampoline', 'assets/trampoline.png', 96, 22);
		
		//Load soundeffects
		game.load.audio('die', ['assets/die.ogg', 'assets/die.mp3']);
		game.load.audio('footstep', ['assets/footstep.ogg', 'assets/footstep.mp3']);
		game.load.audio('hit', ['assets/hit.ogg', 'assets/hit.mp3']);
		game.load.audio('music', ['assets/music.ogg', 'assets/music.mp3']);
		
		//Load menu background
		game.load.image('background', 'assets/background.jpg');
	},
	create: function() {
		game.state.start('menu');
	}
};