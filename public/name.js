var nameState = {
	create: function() {
		document.getElementById('nameInput').style.display = 'block';
		game.add.image(0,0,'background');
		
		var finalscoreLabel = game.add.text(game.width/2,100,'Your score is '+game.global.score,{font: '40px Verdana', fontStyle: 'italic bold', fill: '#cc00cc'});
		finalscoreLabel.anchor.setTo(0.5,0.5);
		
		var textLabel = game.add.text(game.width/2,240,'Enter your name',{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		textLabel.anchor.setTo(0.5,0.5);
	},
	update: function() {
		document.getElementById('btn').addEventListener('click',this.writeScore);
	},
	writeScore: function() {
		var nameVal = document.getElementById("nameVal").value;
		if(nameVal!=""){
			firebase.database().ref('/scoreboard/').push().set({
				name: nameVal,
				score: game.global.score
			}).then(function(){
				console.log("success");
			}).catch(function(e){
				console.log(e.message);
			});
			game.state.start('score');
		}
		else {
			alert("Please enter your name!");
		}
	},
}