var scoreState = {
	create: function() {
		document.getElementById('nameInput').style.display = 'none';

		var index = 5;
		firebase.database().ref('/scoreboard/').orderByChild('score').limitToLast(5).once('value').then(function(snapshot){
			snapshot.forEach(function(childSnapshot){
				var childData = childSnapshot.val();
				if(index==5){
					this.firstLabel = game.add.text(game.width/2,240,'5. ' + childData.name + ' ' + childData.score,{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
					this.firstLabel.anchor.setTo(0.5,0.5);
				}
				else if(index==4){
					this.fourthLabel = game.add.text(game.width/2,210,'4. ' + childData.name + ' ' + childData.score,{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
					this.fourthLabel.anchor.setTo(0.5,0.5);
				}
				else if(index==3){
					this.thirdLabel = game.add.text(game.width/2,180,'3. ' + childData.name + ' ' + childData.score,{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
					this.thirdLabel.anchor.setTo(0.5,0.5);
				}
				else if(index==2){
					this.secondLabel = game.add.text(game.width/2,150,'2. ' + childData.name + ' ' + childData.score,{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
					this.secondLabel.anchor.setTo(0.5,0.5);
				}
				else if(index==1){
					this.firstLabel = game.add.text(game.width/2,120,'1. ' + childData.name + ' ' + childData.score,{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
					this.firstLabel.anchor.setTo(0.5,0.5);
				}
				index -= 1;
				console.log(childData.name+" "+childData.score);
				console.log(index);
			});
		}).catch(e => console.log(e.message));

		var nameLabel = game.add.text(game.width/2,50,'SCOREBOARD',{font: '50px Verdana', fontStyle: 'italic bold', fill: '#cc00cc'});
		nameLabel.anchor.setTo(0.5,0.5);
		
		this.startLabel = game.add.text(game.width/2,300,'Press "Enter" to start',{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		this.startLabel.anchor.setTo(0.5,0.5);
		this.menuLabel = game.add.text(game.width/2,340,'Press "Space" for menu',{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		this.menuLabel.anchor.setTo(0.5,0.5);

		var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
		enterKey.onDown.add(this.start,this);
		var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		spaceKey.onDown.add(this.menu,this);
	},
	start: function() {
		game.state.start('play');
	},
	menu: function() {
		game.state.start('menu');
	}
}