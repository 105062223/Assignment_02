var menuState = {
	create: function() {
		game.add.image(0,0,'background');
		
		var nameLabel = game.add.text(game.width/2,100,'NS-SHAFT',{font: '50px Verdana', fontStyle: 'italic bold', fill: '#cc00cc'});
		nameLabel.anchor.setTo(0.5,0.5);
		
		this.instructLabel = game.add.text(game.width/2,220,'Press left and right arrow keys to move.',{font: '15px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		this.instructLabel.anchor.setTo(0.5,0.5);

		this.startLabel = game.add.text(game.width/2,300,'Press "Enter" to start',{font: '25px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		this.startLabel.anchor.setTo(0.5,0.5);
		this.timer = 0;

		this.scoreLabel = game.add.text(game.width/2,340,'Press "Space" for scoreboard',{font: '20px Verdana', fontStyle: 'bold', fill: '#cc00cc'});
		this.scoreLabel.anchor.setTo(0.5,0.5);
		
		var enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
		enterKey.onDown.add(this.start,this);
		var spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
		spaceKey.onDown.add(this.scoreboard,this);
	},
	start: function() {
		game.state.start('play');
	},
	scoreboard: function() {
		game.state.start('score');
	},
	update: function() {
		this.timer += game.time.elapsed;
		if(this.timer >= 500){
			this.timer -= 500;
			this.startLabel.visible = !this.startLabel.visible;
		}
	}
};