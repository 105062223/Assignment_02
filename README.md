# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed

## Report
1. **Game process**: boot => load => menu(按Enter開始遊戲，按Spacebar到排行榜) => game => name(讓使用者輸入名字) => scoreboard(按Enter開始遊戲，按Spacebar到menu)
2. **Gameplay**: 有四種平台，分別是一般、尖刺、輸送帶和彈簧，碰到最上方的尖刺和尖刺平台會扣5滴血，每踩到一次其他平台會補1滴血。0滴血或掉出視窗時死亡。採到尖刺或碰到最上方尖刺有cameraflash，每隔3秒目前的Level數加1
3. **Sound**: 有背景音樂以及踩到平台、採到尖刺和掉出視窗的音效
4. **Scoreboard**: 玩完遊戲後要輸入名字傳到firebase database，排行榜顯示前5名
5. **Physics**: Player和平台、兩側牆壁以及上方尖刺的碰撞，Player的gravity